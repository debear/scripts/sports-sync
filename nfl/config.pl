#!/usr/bin/perl -w
# NFL automated processing configuration

# Load global config
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

##
## Daily config
##
%{$config{'daily'}} = (
  'app' => 'sports_nfl',
  'daily_tunnel' => 1,
  # Steps to run
  'steps' => [
    ##
    ## Sports Calcs
    ##
    # Rosters
    { 'label' => 'NFL: Rosters',
      'script' => "$config{'sports_dir'}/update_rosters.pl" },
    # Games
    { 'label' => 'NFL: Games',
      'script' => "$config{'sports_dir'}/update_games.pl --unattended" },
    # Upload Database
    { 'label' => 'NFL: Upload Database',
      'script' => "$config{'syncbin_dir'}/server-sync sports_nfl live --daily-tunnel --benchmark" },
    ##
    ## Fantasy Record Breakers
    ##
    # Download Database
    { 'label' => 'Fantasy: Download Database',
      'script' => "$config{'syncbin_dir'}/server-sync fantasy_records_nfl live --daily-tunnel --benchmark" },
    # Process
    { 'label' => 'Fantasy: Record Breakers',
      'script' => "$config{'fantasy_dir'}/records/process.pl nfl" },
    # Upload Database
    { 'label' => 'Fantasy: Upload Database',
      'script' => "$config{'syncbin_dir'}/server-sync fantasy_records_nfl_scoring live --daily-tunnel --benchmark" },
  ],
  'email-subject' => "Automated daily NFL script for $config{'proc_date'}",
);

##
## Injuries config
##
%{$config{'injuries'}} = (
  'app' => 'sports_nfl_inj',
  # Steps to run
  'steps' => [
    # Injury Reports
    { 'label' => 'NFL: Injury Reports',
      'script' => "$config{'sports_dir'}/update_injuries.pl" },
    # Upload Database
    { 'label' => 'NFL: Upload Database',
      'script' => "$config{'syncbin_dir'}/server-sync sports_nfl_injuries live --benchmark" },
  ],
  'email-subject' => "Automated NFL injury script for $config{'proc_date'}",
  'log_inc_time' => '%H%M',
);

##
## Inactives config
##
%{$config{'inactives'}} = (
  'app' => 'sports_nfl_inct',
  # Steps to run
  'steps' => [
    # Injury Reports
    { 'label' => 'NFL: Inactives List',
      'script' => "$config{'sports_dir'}/update_inactives.pl" },
    # Upload Database
    { 'label' => 'NFL: Upload Database',
      'script' => "$config{'syncbin_dir'}/server-sync sports_nfl_injuries live --benchmark" },
  ],
  'email-subject' => "Automated NFL inactive script for $config{'proc_date'}",
  'log_inc_time' => '%H%M',
);

##
## Player Mugshot config
##
%{$config{'mugshots'}} = (
  'app' => 'sports_nfl_mugshots',
  # Steps to run
  'steps' => [
    # Injury Reports
    { 'label' => 'NFL: Mugshot Resync',
      'script' => "$config{'sports_dir'}/update_mugshots.pl" },
    # Upload Database
    { 'label' => 'NFL: Upload Database',
      'script' => "$config{'syncbin_dir'}/server-sync sports_nfl_players live --benchmark" },
  ],
  'email-subject' => "Automated NFL mugshot resync script for $config{'proc_date'}",
);

# Return true to pacify the compiler
1;
