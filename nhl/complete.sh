# Complete a run following Player Linkage problem
dir_base=$(dirname $0)
dir_root=$(realpath "$dir_base/../../..")
dir_data="$dir_root/sites/sports/data/nhl"

# Run as completing script
$dir_data/update_games.pl --complete --log-dir="$(date +'%F').cmpl"

# Then perform the post-processing steps
$dir_base/complete.pl
