#!/usr/bin/perl -w
# NHL automated processing configuration

use Storable qw(dclone);

# Load global config
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

##
## Daily config
##
%{$config{'daily'}} = (
  'app' => 'sports_nhl',
  'daily_tunnel' => 1,
  # Steps to run
  'steps' => [
    ##
    ## Sports Calcs
    ##
    # Schedule
    { 'label' => 'NHL: Schedule',
      'script' => "$config{'sports_dir'}/update_schedule.pl --unattended" },
    # Games
    { 'label' => 'NHL: Games',
      'script' => "$config{'sports_dir'}/update_games.pl --unattended" },
    # Upload Database
    { 'label' => 'NHL: Upload Database',
      'script' => "$config{'syncbin_dir'}/server-sync sports_nhl live --daily-tunnel --benchmark" },
    ##
    ## Fantasy Record Breakers
    ##
    # Download Database
    { 'label' => 'Fantasy: Download Database',
      'script' => "$config{'syncbin_dir'}/server-sync fantasy_records_nhl live --daily-tunnel --benchmark" },
    # Process
    { 'label' => 'Fantasy: Record Breakers',
      'script' => "$config{'fantasy_dir'}/records/process.pl nhl" },
    # Upload Database
    { 'label' => 'Fantasy: Upload Database',
      'script' => "$config{'syncbin_dir'}/server-sync fantasy_records_nhl_scoring live --daily-tunnel --benchmark" },
  ],
  'email-subject' => "Automated daily NHL script for $config{'proc_date'}",
);

##
## Completing the daily update, following manual roster fix
## - This is a modified version of the 'daily' script
##
%{$config{'complete'}} = %{ dclone($config{'daily'}) };
@{$config{'complete'}{'steps'}} = splice(@{$config{'complete'}{'steps'}}, 2); # Drop the completed, and must-be-run-manually steps
$config{'complete'}{'email-subject'} =~ s/^Automated daily/Daily completion/;

##
## Starting Lineups config
##
%{$config{'lineups'}} = (
  'app' => 'sports_nhl_lineup',
  # Steps to run
  'steps' => [
    # Injury Reports
    { 'label' => 'NHL: Starting Lineups',
      'script' => "$config{'sports_dir'}/update_lineups.pl" },
    # Upload Database
    { 'label' => 'NHL: Upload Database',
      'script' => "$config{'syncbin_dir'}/server-sync sports_nhl_lineup live --benchmark" },
  ],
  'email-subject' => "Automated NHL starting lineups script for $config{'proc_date'}",
  'log_inc_time' => '%H%M',
);

##
## Player Mugshot config
##
%{$config{'mugshots'}} = (
  'app' => 'sports_nhl_mugshots',
  # Steps to run
  'steps' => [
    # Injury Reports
    { 'label' => 'NHL: Mugshot Resync',
      'script' => "$config{'sports_dir'}/update_mugshots.pl" },
    # Upload Database
    { 'label' => 'NHL: Upload Database',
      'script' => "$config{'syncbin_dir'}/server-sync sports_nhl_players live --benchmark" },
  ],
  'email-subject' => "Automated NHL mugshot resync script for $config{'proc_date'}",
);

# Return true to pacify the compiler
1;
