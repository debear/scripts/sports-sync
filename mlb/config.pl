#!/usr/bin/perl -w
# MLB automated processing configuration

# Load global config
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

##
## Daily config
##
%{$config{'daily'}} = (
  'app' => 'sports_mlb',
  'daily_tunnel' => 1,
  # Steps to run
  'steps' => [
    ##
    ## Sports Calcs
    ##
    # Schedule
    { 'label' => 'MLB: Schedule',
      'script' => "$config{'sports_dir'}/update_schedule.pl --unattended" },
    # Rosters
    { 'label' => 'MLB: Rosters / Depth Charts',
      'script' => "$config{'sports_dir'}/update_rosters.pl" },
    # Injuries
    #{ 'label' => 'MLB: Injuries',
    #  'script' => "$config{'sports_dir'}/update_injuries.pl" },
    # Games
    { 'label' => 'MLB: Games',
      'script' => "$config{'sports_dir'}/update_games.pl --unattended" },
    # Probables
    { 'label' => 'MLB: Probables',
      'script' => "$config{'sports_dir'}/update_probables.pl" },
    # Upload Database
    { 'label' => 'MLB: Upload Database',
      'script' => "$config{'syncbin_dir'}/server-sync sports_mlb live --daily-tunnel --benchmark" },
    ##
    ## Fantasy Record Breakers
    ##
    # Download Database
    { 'label' => 'Fantasy: Download Database',
      'script' => "$config{'syncbin_dir'}/server-sync fantasy_records_mlb live --daily-tunnel --benchmark" },
    # Process
    { 'label' => 'Fantasy: Record Breakers',
      'script' => "$config{'fantasy_dir'}/records/process.pl mlb" },
    # Upload Database
    { 'label' => 'Fantasy: Upload Database',
      'script' => "$config{'syncbin_dir'}/server-sync fantasy_records_mlb_scoring live --daily-tunnel --benchmark" },
  ],
  'email-subject' => "Automated daily MLB script for $config{'proc_date'}",
);

##
## Starting Lineups config
##
%{$config{'lineups'}} = (
  'app' => 'sports_mlb_lineup',
  # Steps to run
  'steps' => [
    # Injury Reports
    { 'label' => 'MLB: Starting Lineups',
      'script' => "$config{'sports_dir'}/update_lineups.pl" },
    # Upload Database
    { 'label' => 'MLB: Upload Database',
      'script' => "$config{'syncbin_dir'}/server-sync sports_mlb_lineup live --benchmark" },
  ],
  'email-subject' => "Automated MLB starting lineups script for $config{'proc_date'}",
  'log_inc_time' => '%H%M',
);

##
## Player Mugshot config
##
%{$config{'mugshots'}} = (
  'app' => 'sports_mlb_mugshots',
  # Steps to run
  'steps' => [
    # Injury Reports
    { 'label' => 'MLB: Mugshot Resync',
      'script' => "$config{'sports_dir'}/update_mugshots.pl" },
    # Upload Database
    { 'label' => 'MLB: Upload Database',
      'script' => "$config{'syncbin_dir'}/server-sync sports_mlb_players live --benchmark" },
  ],
  'email-subject' => "Automated MLB mugshot resync script for $config{'proc_date'}",
);

# Return true to pacify the compiler
1;
