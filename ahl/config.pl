#!/usr/bin/perl -w
# AHL automated processing configuration

# Load global config
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

##
## Daily config
##
%{$config{'daily'}} = (
  'app' => 'sports_ahl',
  'daily_tunnel' => 1,
  # Steps to run
  'steps' => [
    ##
    ## Sports Calcs
    ##
    # Transaction
    { 'label' => 'AHL: Transactions',
      'script' => "$config{'sports_dir'}/update_transactions.pl --unattended" },
    # Schedule
    { 'label' => 'AHL: Schedule',
      'script' => "$config{'sports_dir'}/update_schedule.pl --unattended" },
    # Games
    { 'label' => 'AHL: Games',
      'script' => "$config{'sports_dir'}/update_games.pl --unattended" },
    # Upload Database
    { 'label' => 'AHL: Upload Database',
      'script' => "$config{'syncbin_dir'}/server-sync sports_ahl live --daily-tunnel --benchmark" },
    ##
    ## Fantasy Record Breakers
    ##
    # Download Database
    { 'label' => 'Fantasy: Download Database',
      'script' => "$config{'syncbin_dir'}/server-sync fantasy_records_ahl live --daily-tunnel --benchmark" },
    # Process
    { 'label' => 'Fantasy: Record Breakers',
      'script' => "$config{'fantasy_dir'}/records/process.pl ahl" },
    # Upload Database
    { 'label' => 'Fantasy: Upload Database',
      'script' => "$config{'syncbin_dir'}/server-sync fantasy_records_ahl_scoring live --daily-tunnel --benchmark" },
  ],
  'email-subject' => "Automated daily AHL script for $config{'proc_date'}",
);

##
## Player Mugshot config
##
%{$config{'mugshots'}} = (
  'app' => 'sports_ahl_mugshots',
  # Steps to run
  'steps' => [
    # Injury Reports
    { 'label' => 'AHL: Mugshot Resync',
      'script' => "$config{'sports_dir'}/update_mugshots.pl" },
    # Upload Database
    { 'label' => 'AHL: Upload Database',
      'script' => "$config{'syncbin_dir'}/server-sync sports_ahl_players live --benchmark" },
  ],
  'email-subject' => "Automated AHL mugshot resync script for $config{'proc_date'}",
);

# Return true to pacify the compiler
1;
