#!/usr/bin/perl -w
# F1 processing configuration

# Load global config
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

##
## Fantasy Calc config
##
%{$config{'fantasy'}} = (
  'app' => 'sports_f1_fantasy',
  'daily_tunnel' => 1,
  # Steps to run
  'steps' => [
    ##
    ## Fantasy Record Breakers
    ##
    # Download Database
    { 'label' => 'Fantasy: Download Database',
      'script' => "$config{'syncbin_dir'}/server-sync fantasy_records_f1 live --daily-tunnel --benchmark" },
    # Process
    { 'label' => 'Fantasy: Record Breakers',
      'script' => "$config{'fantasy_dir'}/records/process.pl f1 --preview" },
    # Upload Database
    { 'label' => 'Fantasy: Upload Database',
      'script' => "$config{'syncbin_dir'}/server-sync fantasy_records_f1_scoring live --daily-tunnel --benchmark" },
  ],
  'email-subject' => "Automated Fantasy F1 script for $config{'proc_date'}",
);

##
## Race processing config
##
%{$config{'process'}} = (
  'app' => 'sports_f1',
  # Steps to run
  'steps' => [
    ##
    ## Sports Calcs
    ##
    # Race Processing
    { 'label' => 'F1: Race',
      'script' => "$config{'sports_dir'}/update_race.pl --unattended" },
    # Upload Sports Database
    { 'label' => 'F1: Upload Database',
      'script' => "$config{'syncbin_dir'}/server-sync sports_f1 live --benchmark" },
    ##
    ## Fantasy Record Breakers
    ##
    # Download Database
    { 'label' => 'Fantasy: Download Database',
      'script' => "$config{'syncbin_dir'}/server-sync fantasy_records_f1 live --daily-tunnel --benchmark" },
    # Process
    { 'label' => 'Fantasy: Record Breakers',
      'script' => "$config{'fantasy_dir'}/records/process.pl f1" },
    # Upload Database
    { 'label' => 'Fantasy: Upload Database',
      'script' => "$config{'syncbin_dir'}/server-sync fantasy_records_f1_scoring live --daily-tunnel --benchmark" },
  ],
  'email-subject' => "F1 race processing script for $config{'proc_date'}",
  'log_inc_time' => '%H%M',
);

# Return true to pacify the compiler
1;
