#!/usr/bin/perl -w
# General processing configuration

use MIME::Base64;
use File::Basename;
use File::Path qw(make_path);

our $mode;
my $sport = basename(dirname($0));
my $base_dir = '/var/www/debear';
our %config = (
  'domain' => 'debear.uk',
  'log_dir' => "$base_dir/logs/common/sports-sync/$sport" . (defined($mode) ? "/$mode" : ''),
  'sports_dir' => "$base_dir/sites/sports/data/$sport",
  'fantasy_dir' => "$base_dir/sites/fantasy/data",
  'syncbin_dir' => "$base_dir/bin/git-admin",
  # Database details
  'db_admin' => 'debearco_admin',
  'db_common' => 'debearco_common',
  'db_sports' => 'debearco_sports',
  'db_fantasy' => 'debearco_fantasy',
  'db_user' => 'debearco_sysadmn',
  # Exit Codes
  'exit_codes' => {
    'okay' => 0,
    # Pre-requisite checks
    'failed-prereq' => 98,
    'failed-prereq-silent' => 1, # Same as above, but does not trigger a warning
    # Generic error
    'unknown' => 99,
    # Pre-processing
    'pre-flight' => 10,
    # Core part of script
    'core' => 20,
    # Arbitrarily grouped sections of post-processing
    'post-process-1' => 31,
    'post-process-2' => 32,
    'post-process-3' => 33,
    'post-process-4' => 34,
    'post-process-5' => 35,
  },
);
$config{'proc_date'} = `date +'%F'`; chomp($config{'proc_date'});

# Database Password
$config{'db_pass'} = do { local(@ARGV, $/) = "$base_dir/etc/passwd/web/db"; <> }; chomp($config{'db_pass'});
$config{'db_pass'} = decode_base64($config{'db_pass'});

# Ensure the log dir exists
make_path($config{'log_dir'})
  if ! -e $config{'log_dir'};

# Return true to pacify the compiler
1;
