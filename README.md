# Sports-Sync Scripts

Wrapper scripts for processing sports data that affects both Sports and Fantasy, and so contains scripts across both repos.

## AHL
* Daily - Automated processing of the previous day's games, schedules, rosters, etc
* Mugshots - Weekly processing of new player mugshots

## NHL
* Daily - Automated processing of the previous day's games, schedules, rosters, etc
* Complete - Manual completion of the "daily" script, when user input is required
* Lineups - Processing the starting lineups of upcoming games
* Mugshots - Weekly processing of new player mugshots

## NFL
* Daily - Automated processing of the previous day's games, schedules, rosters, etc
* Injuries - Processing the weekly Injury reports
* Inactives - Processing the inactives of upcoming games
* Mugshots - Weekly processing of new player mugshots

## MLB
* Daily - Automated processing of the previous day's games, schedules, rosters, etc
* Lineups - Processing the starting lineups of upcoming games
* Mugshots - Weekly processing of new player mugshots

## F1
* Fantasy - Automated preview calculations for Fantasy games
* Process - Manual race processing

## SGP
* Fantasy - Automated preview calculations for Fantasy games
* Process - Manual meeting processing

## Status

[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

## Authors

* **Thierry Draper** - [@ThierryDraper](https://gitlab.com/thierrydraper)

## Contribute

Please read [CONTRIBUTING.md](https://gitlab.com/debear/scripts/sports-sync/blob/master/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## License

This project is made available under the terms of the GNU Affero General Public License v3.0. Please see the [LICENSE](https://gitlab.com/debear/scripts/sports-sync/blob/master/LICENSE) file for details.

GNU AGPLv3 © [Thierry Draper](https://gitlab.com/thierrydraper)
