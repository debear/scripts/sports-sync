#!/usr/bin/perl -w
# SGP processing configuration

# Load global config
my $config = abs_path(__DIR__ . '/../config.pl');
require $config;

##
## Daily config
##
%{$config{'fantasy'}} = (
  'app' => 'sports_sgp_fantasy',
  'daily_tunnel' => 1,
  # Steps to run
  'steps' => [
    ##
    ## Fantasy Record Breakers
    ##
    # Download Database
    { 'label' => 'Fantasy: Download Record Breakers',
      'script' => "$config{'syncbin_dir'}/server-sync fantasy_records_sgp live --daily-tunnel --benchmark" },
    # Process
    { 'label' => 'Fantasy: Record Breakers',
      'script' => "$config{'fantasy_dir'}/records/process.pl sgp --preview" },
    # Upload Database
    { 'label' => 'Fantasy: Upload Record Breakers',
      'script' => "$config{'syncbin_dir'}/server-sync fantasy_records_sgp_scoring live --daily-tunnel --benchmark" },
  ],
  'email-subject' => "Automated Fantasy SGP script for $config{'proc_date'}",
);

##
## Meeting processing config
##
%{$config{'process'}} = (
  'app' => 'sports_sgp_meeting',
  # Steps to run
  'steps' => [
    ##
    ## Sports Calcs
    ##
    # Race Processing
    { 'label' => 'SGP: Race',
      'script' => "$config{'sports_dir'}/update_meeting.pl --unattended" },
    # Upload Sports Database
    { 'label' => 'SGP: Upload Database',
      'script' => "$config{'syncbin_dir'}/server-sync sports_sgp live --benchmark" },
    ##
    ## Fantasy Record Breakers
    ##
    # Download Database
    { 'label' => 'Fantasy: Download Record Breakers',
      'script' => "$config{'syncbin_dir'}/server-sync fantasy_records_sgp live --daily-tunnel --benchmark" },
    # Process
    { 'label' => 'Fantasy: Record Breakers',
      'script' => "$config{'fantasy_dir'}/records/process.pl sgp" },
    # Upload Database
    { 'label' => 'Fantasy: Upload Record Breakers',
      'script' => "$config{'syncbin_dir'}/server-sync fantasy_records_sgp_scoring live --daily-tunnel --benchmark" },
  ],
  'email-subject' => "SGP meeting processing script for $config{'proc_date'}",
  'log_inc_time' => '%H%M',
);

# Return true to pacify the compiler
1;
